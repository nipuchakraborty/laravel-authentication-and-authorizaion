<?php

namespace App\Http\Controllers\Api;


use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class AuthurizationController extends Controller
{
 public function createRole(Request $request){
     $role=Role::create($request->name);
     $permission = Permission::create(['name' => $request->name]);
     $role->givePermissionTo($permission);
     $permission->assignRole($role);
 }

 public function giveMultipePermission(Request $request, $id){
     $roles=Role::find($id);
     $permissions = Permission::create(['name' => $request->name]);
     $roles->syncPermissions($permissions);
     $permissions->syncRoles($roles);
 }

 public function removePermission(Request $request,$id){
     $role=Role::find($id);
     $permission=Permission::Where("permission",$request->permissions);
     $role->revokePermissionTo($request->permssions);
     $permission->removeRole($role);
 }


//
// public function userPermission(){
//     $role = Role::create(['name' => 'writer']);
//     $permission = Permission::create(['name' => 'edit articles']);
//     $role->givePermissionTo($permission);
//     $permission->assignRole($role);
//     $role->syncPermissions($permissions);
//     $permission->syncRoles($roles);
//     $role->revokePermissionTo($permission);
//     $permission->removeRole($role);
//     $permissionNames = $user->getPermissionNames(); // collection of name strings
//     $permissions = $user->permissions; // collection of permission objects
//
//// get all permissions for the user, either directly, or from roles, or from both
//     $permissions = $user->getDirectPermissions();
//     $permissions = $user->getPermissionsViaRoles();
//     $permissions = $user->getAllPermissions();
//
//// get the names of the user's roles
//     $roles = $user->getRoleNames(); // Returns a collection
//     $users = User::role('writer')->get(); // Returns only users with the role 'writer'
//     $users = User::permission('edit articles')->get(); // Returns only users with the permission 'edit articles' (inherited or directly)
//     $all_users_with_all_their_roles = User::with('roles')->get();
//     $all_users_with_all_direct_permissions = User::with('permissions')->get();
//     $all_roles_in_database = Role::all()->pluck('name');
// }

 public function getPage(){
     $routeList = Route::getRoutes();
     $route=[];

     var_dump($routeList);

 }




}
