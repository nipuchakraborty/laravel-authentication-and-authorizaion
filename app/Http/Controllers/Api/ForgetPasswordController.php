<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgetPasswordController extends Controller
{
  use SendsPasswordResetEmails;
  protected function sendResetLinkResponse(Request $request,$reponse)
  {
    return response(["message"=>$reponse]);
  }
  protected function sendResetLinkFailedResponse(Request $request, $response)
  {
    return response(['error'=>$response],422);
  }
}
