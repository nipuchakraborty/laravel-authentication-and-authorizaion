<?php

use App\Notifications\PasswordResetNotification;
use App\Task;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Migrations\Migration;
use Laravel\Passport\HasApiTokens;

class CreateUserTable extends Migration
{
    use Notifiable, HasApiTokens;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }
}
