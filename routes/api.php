<?php
use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/role','Api\AuthurizationController@createRole');
    Route::post('/permission','Api\AuthurizationController@createPermission');
    Route::get('/page','Api\AuthurizationController@getPage');
});



Route::post('/register',"Api\AuthController@register");
Route::post('/login',"Api\AuthController@login");
Route::post('/password/reset', 'Api\ResetPasswordController@reset');


